#include<stdio.h>

int main()
{
	char c;

	printf("Enter a letter : ");
	scanf("%c", &c);

	if(c == 'A' || c == 'a' || c == 'E' || c == 'e' || c == 'I' || c == 'i' || c == 'O' || c == 'o' || c == 'U' || c == 'u')
	{
		printf("%c is a Vowel\n", c);
	}
	else
	{
		printf("%c is a Consonant\n", c);
	}

	return 0;
}
